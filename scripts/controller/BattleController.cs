﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleController : StateMachine {
	public CameraController cameraController;
	public Level level;
	public static Dictionary<Point, Tile> tiles;
	public static List<BatObj> batObjs;
	public static List<BatObj> destroyedBatObjs = new List<BatObj>();

	public Tile hoveredTile = null;
	public GameObject hover;
	public BatObj activeUnit = null;

	void Start () {
		UnitData char1 = new UnitData();
		UnitData char2 = new UnitData();
		char1.name = "A";
		char1.modelNames = new Dictionary<string, string>() {{"HUM1", "PUH1"}, {"HAIR", "HAIR"}, {"SWD1", "SWD1"}};
		char1.maxHP = 30;
		char1.HP = 30;
		char2.name = "B";
		char2.modelNames = new Dictionary<string, string>() {{"HUM1", "PUH1"}, {"HAIR", "HAIR"}, {"AXE1", "AXE1"}};
		char2.maxHP = 30;
		char2.HP = 30;
		SaveState.characters.Add ("A", char1);
		SaveState.characters.Add ("B", char2);

		if (hover == null) {
			hover = Instantiate(Resources.Load ("Prefabs/Hover"), new Vector3(0, -100, 0), Quaternion.Euler (90f, 0f, 0f)) as GameObject;
		}

		ChangeState<InitBattleState>();
	}

	public static void KillUnit(BatObj unit) {
		BattleController.destroyedBatObjs.Add(unit);
		BattleController.batObjs.Remove (unit);
	}
}
