﻿using UnityEngine;
using System.Collections;

public class SelectGrimoireBattleState : BattleState {

	public override void Enter () {
		base.Enter ();
		Debug.Log ("Grimoire");
	}

	protected override void OnKeyDownEvent (object sender, InfoEventArgs<string> e) {
		base.OnKeyDownEvent(sender, e);
		if (e.info == "back") {
			if (!activeUnit.acted) {
				owner.ChangeState<SelectActionBattleState>();
			}
		}
		if (e.info == "confirm") {
			activeUnit.acted = true;
			owner.ChangeState<SelectActionBattleState>();
		}
	}
}
