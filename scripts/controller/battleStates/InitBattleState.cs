﻿using UnityEngine;
using System.Collections;

public class InitBattleState : BattleState {
	public override void Enter () {
		base.Enter ();
		StartCoroutine(Init());
	}
	
	IEnumerator Init () {
		level.LoadLevelData("TestLevel");
		level.LoadSpeechData("TestLevel");
		cameraController.Init();
		yield return null;
		owner.ChangeState<SpeechBattleState>();
	}
}