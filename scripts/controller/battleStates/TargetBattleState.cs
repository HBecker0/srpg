﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TargetBattleState : BattleState {
	public List<Point> targets;
	Actor actor;
	bool returnFlag = false;

	public override void Enter () {
		base.Enter ();
		targets = genTargets();
		foreach (Point point in targets) {
			BattleController.tiles[point].SetOverlay ("actionTarget");
		}
		actor = activeUnit.GetComponent<Actor>() as Actor;
	}

	public override void Exit() {
		base.Exit ();
		if (targets != null && !activeUnit.acted) {
			foreach (Point point in targets) {
				BattleController.tiles[point].SetOverlay ("base");
			}
			activeUnit.GetTile ().SetOverlay ("active");
		}
	}

	protected override void OnKeyDownEvent (object sender, InfoEventArgs<string> e) {
		base.OnKeyDownEvent(sender, e);
		if (e.info == "back") {
			if (!activeUnit.acted) {
				owner.ChangeState<SelectActionBattleState>();
			}
		}
	}

	protected override void OnMouseButtonDownEvent (object sender, InfoEventArgs<int> e) {
		base.OnMouseButtonDownEvent(sender, e);
		if (e.info == 0) {
			if (hoveredTile != null) {
				if (targets.Contains (hoveredTile.pos)) {
					foreach (Point point in targets) {
						BattleController.tiles[point].SetOverlay ("base");
					}
					activeUnit.GetTile ().SetOverlay ("base");
					StartCoroutine(actor.Act (hoveredTile.pos));
					activeUnit.acted = true;
					returnFlag = true;
				}
			}
		}
	}

	void Update() {
		if (returnFlag && !actor.active) {
			returnFlag = false;
			owner.ChangeState<SelectActionBattleState>();
		}
	}

	public List<Point> genTargets() {
		List<Point> targetsTemp = RouteSearch.BFS (activeUnit.GetLocation(), activeUnit.currentAction.range, (Point t) => CheckCanPassThrough(t)).Keys.ToList ();
		List<Point> targets = new List<Point>();
		foreach (Point tile in targetsTemp) {
			if (BattleController.tiles[tile].height <= (BattleController.tiles[tile].height + activeUnit.currentAction.height)) { //Tile not too high
				if (BattleController.tiles[tile].transform.childCount == 0 && activeUnit.currentAction.validTargetTypes.Contains ("Empty")) {
					targets.Add (tile);
				}
				for (int i = 0; i < BattleController.tiles[tile].transform.childCount; i++) {
					if (activeUnit.currentAction.validTargetTypes.Contains (BattleController.tiles[tile].transform.GetChild (i).GetComponent<BatObj>().type)) {
						targets.Add (tile);
					}
				}
			}
		}
		return targets;
	}

	public bool CheckCanPassThrough(Point tile) {
		return true; //if tile content/type not in activeAction.blockedBy
	}
}
