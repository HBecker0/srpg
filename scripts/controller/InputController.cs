﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class InputController : MonoBehaviour {
	public static event EventHandler<InfoEventArgs<string>> keyEvent;
	public static event EventHandler<InfoEventArgs<string>> keyDownEvent;
	public static event EventHandler<InfoEventArgs<int>> mouseButtonUpEvent;
	public static event EventHandler<InfoEventArgs<int>> mouseButtonDownEvent;
	public static event EventHandler<InfoEventArgs<Vector2>> mouseAxisEvent;
	public static event EventHandler<InfoEventArgs<float>> mouseScrollEvent;

	void Update () {
		foreach (string button in SaveState.settings.keyBindings.Keys.ToArray()) {
			if (Input.GetKeyDown(SaveState.settings.keyBindings[button])) {
				if (keyDownEvent != null) {
					keyDownEvent(this, new InfoEventArgs<string>(button));
				}
			}
			if (Input.GetKey(SaveState.settings.keyBindings[button])) {
				if (keyEvent != null) {
					keyEvent(this, new InfoEventArgs<string>(button));
				}
			}
		}
		for (int mButton = 1; mButton <= 3; mButton++) {
			if (Input.GetMouseButtonUp (mButton)) {
				if (mouseButtonUpEvent != null) {
					mouseButtonUpEvent(this, new InfoEventArgs<int>(mButton));
				}
			}
		}
		for (int mButton = 0; mButton <= 2; mButton++) {
			if (Input.GetMouseButtonDown (mButton)) {
				if (mouseButtonDownEvent != null) {
					mouseButtonDownEvent(this, new InfoEventArgs<int>(mButton));
				}
			}
		}
		float mouseX = Input.GetAxisRaw("Mouse X");
		float mouseY = Input.GetAxisRaw("Mouse Y");
		if (mouseX != 0 || mouseY != 0) {
			if (mouseAxisEvent != null) {
				mouseAxisEvent(this, new InfoEventArgs<Vector2>(new Vector2(mouseX, mouseY)));
			}
		}
		float scroll = Input.GetAxisRaw ("Mouse ScrollWheel");
		if (scroll != 0) {
			if (mouseScrollEvent != null) {
				mouseScrollEvent(this, new InfoEventArgs<float>(scroll));
			}
		}

	}
}
