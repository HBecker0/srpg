﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

public class Level : MonoBehaviour {
	public static int mapSize = 1;
	private static GameObject unitPrefab;
	public List<Tile> PUStartingTiles = new List<Tile>();
	public Dictionary<string, Queue<SpeechData>> speechData;

	void Awake() {
		unitPrefab = Resources.Load ("prefabs/BatObj") as GameObject;
	}

	public void LoadLevelData(string levelName) {
		if(File.Exists("Assets/Data/Level/"+levelName+".level")) {
			BattleController.tiles = new Dictionary<Point, Tile>();
			FileStream f = File.Open("Assets/Data/Level/"+levelName+".level", FileMode.Open);
			BinaryReader b = new BinaryReader(f);
				
			mapSize = b.ReadBytes (1)[0];

			int noTiles = BitConverter.ToInt32 (b.ReadBytes (4), 0);
			byte[] loadedMapData = b.ReadBytes(noTiles * Tile.tileDataLength);
			
			int noBatObjBytes = BitConverter.ToInt32 (b.ReadBytes (4), 0);
			byte[] loadedBatObjData = b.ReadBytes(noBatObjBytes);

			GenerateMap(loadedMapData);
			GenerateBatObjs(loadedBatObjData);
		}
	}

	public void LoadSpeechData(string levelName) {
		if(File.Exists("Assets/Data/Level/"+levelName+".xml")) {
			speechData = new Dictionary<string, Queue<SpeechData>>();
			Dictionary<string, Sprite> faces = new Dictionary<string, Sprite>();
			foreach (Sprite face in Resources.LoadAll <Sprite>("ui/faces")) {
				faces.Add (face.name, face);
			}
			XmlDocument xmlDoc = new XmlDocument(); // xmlDoc is the new xml document.
			xmlDoc.Load("Assets/Data/Level/"+levelName+".xml"); // load the file.
			XmlNodeList data = xmlDoc.GetElementsByTagName("speech"); // array of the level nodes.

			foreach (XmlNode speech in data) {
				foreach (XmlNode speechSet in speech) {
					XmlNodeList panels = speechSet.ChildNodes;
					Queue<SpeechData> set = new Queue<SpeechData>();
					foreach(XmlNode panel in panels) {
						set.Enqueue (new SpeechData(faces[panel.Name], panel.InnerText));
					}
					speechData.Add (speechSet.Name, set);
				}
			}
		}
	}

	public void GenerateMap(byte[] data) {
		// Generate tiles from the bitdata
		for (int i = 0; i < data.Length; i += 5) {
			Tile tile = Tile.Create(data.Segment (i, 5));
			tile.transform.parent = transform;
			BattleController.tiles.Add (tile.pos, tile);
		}
		// Create the map mesh from the map data
		foreach (Point k in BattleController.tiles.Keys) {
			BattleController.tiles[k].BuildTile();
		}
	}

	public void GenerateBatObjs(byte[] data) {
		//batObjMaterial = Resources.Load ("materials/BatObjMaterial") as Material;
		for (int i = 0; i < data.Length; i += 4) {
			string type = Encoding.UTF8.GetString(data.Segment(i, 2));
			Point loc = new Point(data[i+2]-128, data[i+3]-128);
			if (type == "PU") {
				PUStartingTiles.Add(BattleController.tiles[loc]);
			} else {
				BatObj batObj = BatObj.Create(type, data.Segment(i+4, 8*data[i+4]+2));
				batObj.transform.parent = BattleController.tiles[loc].transform;
				batObj.BuildBatObj();
				batObj.transform.localPosition = Vector3.zero;
				i += 8*data[i+4] + 2;
			}
		}
		BattleController.batObjs = new List<BatObj>(GetComponentsInChildren<BatObj>());
	}
}
