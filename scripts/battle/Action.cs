﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Action {
	public string type = "HealthChange"; // HealthChange, StatusChange etc
	public int value = -15; // Should use ratios of character stats
	public bool turnToTarget = true;
	public string animation = "attacking";
	public int range = 2;
	public int height = 3;
	public List<string> validTargetTypes = new List<string>() {"EU"}; //Empty, EU, PU, NU
	public List<string> blockedBy = new List<string>() {"IT"}; //IT (impassable terrain), EU, PU, NU, Empty
}
