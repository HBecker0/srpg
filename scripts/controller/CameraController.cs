using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : MonoBehaviour {
	public float INIT_CAMERA_ZOOM = 8f;
	public Vector2 INIT_CAMERA_ROTATION = new Vector2(0f, 30f);
	
	public const float cameraKeyMoveSpeed = 10f;
	public const float cameraMouseMoveSpeed = .3f;
	
	public const float cameraKeyRotateSpeedH = 180f;
	public const float cameraKeyRotateSpeedV = 80f;
	public const float cameraMouseRotateSpeedH = 6f;
	public const float cameraMouseRotateSpeedV = 3f;
	public const float cameraMinRotationV = 20f;
	public const float cameraMaxRotationV = 60f;
	
	public const float cameraMinZoom = 5f;
	public const float cameraMaxZoom = 18f;
	public const float cameraKeyZoomSpeed = 12f;
	public const float cameraMouseZoomSpeed = 6f;
	
	public Vector3 cameraTargetPos = new Vector3();
	public Vector2 cameraRotation = new Vector2();
	public float cameraZoom = 0f;

	private bool mouseButton1 = false;
	private bool mouseButton2 = false;

	Transform cameraTransform;
	Transform targetTransform;
	
	public void Init() {
		cameraTransform = transform.GetChild(0).transform;
		targetTransform = this.transform;
		
		moveCameraAbs (new Vector3(0f, 0f, 0f));
		rotateCameraAbs (INIT_CAMERA_ROTATION);
		zoomCameraAbs (INIT_CAMERA_ZOOM);
	}

	#region input handling
	public void KeyEvent (string e) {
		if (e == "cameraZoomIn") {
			zoomCameraRel (-Time.deltaTime * cameraKeyZoomSpeed);
		}
		if (e == "cameraZoomOut") {
			zoomCameraRel (Time.deltaTime * cameraKeyZoomSpeed);
		}
		if (e == "cameraRotateLeft") {
			rotateCameraRel (new Vector2(Time.deltaTime * cameraKeyRotateSpeedH, 0f));
		}
		if (e == "cameraRotateRight") {
			rotateCameraRel (new Vector2(-Time.deltaTime * cameraKeyRotateSpeedH, 0f));
		}
		if (e == "cameraRotateUp") {
			rotateCameraRel (new Vector2(0f, Time.deltaTime * cameraKeyRotateSpeedV));
		}
		if (e == "cameraRotateDown") {
			rotateCameraRel (new Vector2(0f, -Time.deltaTime * cameraKeyRotateSpeedV));
		}
		
		Vector3 dir = (targetTransform.position - cameraTransform.position).normalized * cameraKeyMoveSpeed * Time.deltaTime;
		if (e == "cameraMoveUp") {
			moveCameraRel (new Vector3(dir.x, 0f, dir.z));
		}
		if (e == "cameraMoveDown") {
			moveCameraRel (new Vector3(-dir.x, 0f, -dir.z));
		}
		if (e == "cameraMoveLeft") {
			moveCameraRel (new Vector3(-dir.z, 0f, dir.x));
		}
		if (e == "cameraMoveRight") {
			moveCameraRel (new Vector3(dir.z, 0f, -dir.x));
		}
	}

	public void MouseButtonUpEvent (int e) {
		if (e == 1) {
			mouseButton1 = false;
		}
		if (e == 2) {
			mouseButton2 = false;
		}
	}

	public void MouseButtonDownEvent (int e) {
		if (e == 1) {
			mouseButton1 = true;
		}
		if (e == 2) {
			mouseButton2 = true;
		}
	}

	public void MouseAxisEvent (Vector2 e) {
		if (mouseButton1) {
			Vector3 dir = (targetTransform.position - cameraTransform.position).normalized * cameraMouseMoveSpeed;
			moveCameraRel (new Vector3(-dir.z, 0f, dir.x) * e.x);
			moveCameraRel (new Vector3(-dir.x, 0f, -dir.z) * e.y);
		} else
		if (mouseButton2) {
			rotateCameraRel (new Vector2(e.x * cameraMouseRotateSpeedH, -e.y * cameraMouseRotateSpeedV));
		}
	}

	public void MouseScrollEvent (float e) {
		zoomCameraRel (-e * cameraMouseZoomSpeed);
	}
	#endregion
	#region Camera movement

	void moveCameraRel(Vector3 moveAmount) {
		moveCameraAbs (moveAmount + cameraTargetPos);
	}

	void moveCameraAbs(Vector3 target) {
		cameraTargetPos = target;
		float h = Mathf.Sqrt(target.x * target.x + target.z * target.z);
		if (h > (Level.mapSize - 1)) {
			float r = (Level.mapSize - 1) / h;
			cameraTargetPos.x *= r;
			cameraTargetPos.z *= r;
		}
		cameraTargetPos.y = Mathf.Clamp (Mathf.Abs(target.y), 0, 99);
		targetTransform.position = cameraTargetPos;
	}

	void rotateCameraRel(Vector2 rotationAmount) {
		rotateCameraAbs(rotationAmount + cameraRotation);
	}

	void rotateCameraAbs(Vector2 rotation) {
		cameraRotation.x = (rotation.x + 360) % 360;
		cameraRotation.y = Mathf.Clamp (rotation.y, cameraMinRotationV, cameraMaxRotationV);
		targetTransform.rotation = Quaternion.Euler(cameraRotation.y, cameraRotation.x, 0f);
	}

	void zoomCameraRel(float zoomAmount) {
		zoomCameraAbs (zoomAmount + cameraZoom);
	}

	void zoomCameraAbs(float zoom) {
		cameraZoom = Mathf.Clamp (zoom, cameraMinZoom, cameraMaxZoom);
		cameraTransform.localPosition = new Vector3(0f, 0f, -cameraZoom);
	}
	#endregion
}