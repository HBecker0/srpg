﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Turns {
	public BatObj unit;
	public int turnCounter;
	public int baseTurns;
	public Turns(BatObj c, int t) {
		unit = c;
		turnCounter = t;
		baseTurns = t;
	}
	public void resetTurns() {
		turnCounter = baseTurns;
	}
}

public class TurnBattleState : BattleState {
	public const int TURN_QUEUE_LENGTH = 6;
	public const int SPD_MAX = 15;

	public List<BatObj> turnQueue = new List<BatObj>();
	public List<Turns> speedNumbers = new List<Turns>();

	public override void Enter () {
		base.Enter ();
		if (speedNumbers.Count == 0) {
			foreach (var o in BattleController.batObjs) {
				if (o.type == "PU" || o.type == "EU") {
					speedNumbers.Add (new Turns(o, SPD_MAX - o.data.speed));
				}
			}
		} else if (activeUnit != null) {
			activeUnit.GetTile ().SetOverlay("base");
			activeUnit = null;
		}
		StartCoroutine(newTurn());
	}

	IEnumerator newTurn() {
		generateTurns ();
		endTurn ();
		yield return null;
		if (activeUnit.type == "PU") {
			owner.ChangeState<SelectActionBattleState>();
		} else {
			owner.ChangeState<EnemyBattleState>();
		}
	}

	public void generateTurns() {
		while (turnQueue.Count <= TURN_QUEUE_LENGTH) {
			for (int i = 0; i < speedNumbers.Count; i++) {
				speedNumbers[i].turnCounter--;
				if (speedNumbers[i].turnCounter <= 0 && (turnQueue.Count == 0 || (speedNumbers[i].unit != turnQueue[turnQueue.Count-1]))) {
					speedNumbers[i].resetTurns();
					turnQueue.Add (speedNumbers[i].unit);
				}
			}
		}
	}
	public void endTurn() {
		activeUnit = turnQueue[0];
		activeUnit.startPos = null;
		activeUnit.moved = false;
		activeUnit.acted = false;
		if (activeUnit.type == "PU") {
			activeUnit.GetTile ().SetOverlay("active");
		}
		turnQueue.RemoveAt (0);
	}
}