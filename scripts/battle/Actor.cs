﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Actor : MonoBehaviour {
	public bool active = false;

	public IEnumerator Act (Point target) {
		active = true;
		BatObj unit = GetComponent<BatObj>();
		Action action = unit.currentAction;
		unit.animator.SetBool (unit.currentAction.animation, true);
		if (unit.currentAction.turnToTarget) {
			unit.dir = unit.GetLocation().GetDirection(target);
			unit.UpdateRotation();
		}
		while (!GetComponent<BatObj>().animator.GetNextAnimatorStateInfo(0).IsName ("actionComplete")) {
			yield return null;
		}
		// Carry out effects of action
		if (action.type == "HealthChange") {
			BattleController.tiles[target].GetComponentInChildren<BatObj>().ChangeHP(action.value);


		}

		//
		active = false;
		unit.animator.SetBool (unit.currentAction.animation, false);
		unit.GetTile ().SetOverlay ("active");

	}
}
