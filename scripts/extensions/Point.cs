﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public struct Point {
	// A data structure containing two points (a, b) representing hexagonal coordinates
	public int a, b;
	public Point(int _a, int _b) {
		a = _a;
		b = _b;
	}
	public Point(float _a, float _b) {
		a = (int)_a;
		b = (int)_b;
	}

	public static Point operator +(Point loc1, Point loc2) {
		return new Point(loc1.a+loc2.a, loc1.b+loc2.b);
	}
	public static Point operator -(Point loc1, Point loc2) {
		return new Point(loc1.a-loc2.a, loc1.b-loc2.b);
	}
	public static bool operator ==(Point loc1, Point loc2) {
		return (loc1.a==loc2.a) && (loc1.b==loc2.b);
	}
	public static bool operator !=(Point loc1, Point loc2) {
		return (loc1.a!=loc2.a) || (loc1.b!=loc2.b);
	}
	public bool Equals(Point point2) {
		if (point2 == null) {
			return false;
		} else if (a == point2.a && b == point2.b) {
			return true;
		}
		return false;
	}
}

public static class PointExtensions {
	public static Vector3 toPosition(this Point p) {
		// Convert to cartesian coordinates
		return new Vector3(p.a - 0.5f * p.b, 0, 0.8655f * p.b);
	}
}

public static class Vector2Extensions {
	public static Vector2 xz (this Vector3 v) {
		return new Vector2(v.x, v.z);
	}
}

public static class Vector3Extensions {
	public static Point toLocation(this Vector3 _pos) {
		// Convert cartesian coordinates to the tile located at them
		float nb = Mathf.RoundToInt(1.154f * _pos.z);
		float na = Mathf.RoundToInt(_pos.x + nb/2);
		Point pos = new Point (na, nb);
		float minDistance = Vector2.Distance (_pos.xz (), pos.toPosition().xz());
		for (int i = 0; i < 6; i++) {
			Point newPos = new Point(na + Tile.dirs[i, 0], nb + Tile.dirs[i, 1]);
			float newDistance = Vector2.Distance (_pos.xz (), newPos.toPosition().xz ());
			if ((newDistance < minDistance) && BattleController.tiles.ContainsKey(newPos)) {
				minDistance = newDistance;
				pos = newPos;
			}
		}
		return pos;
	}
}