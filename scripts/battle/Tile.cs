﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Tile : MonoBehaviour {
	public static Dictionary<string, Material> overlayMats = new Dictionary<string, Material>();
	public static int[,] dirs = new int[6, 2]{{1, 1}, {1, 0}, {0, -1}, {-1, -1}, {-1, 0}, {0, 1}};
	public static int tileDataLength = 5;
	
	public Point pos;
	public int height;
	public bool passable;

	private static GameObject prefab = Resources.Load ("Prefabs/Tile") as GameObject;
	
	private Mesh mesh;
	private MeshCollider meshCollider;
	private Vector3[] hexagonPoints = new Vector3[] {
		new Vector3 (0f, 0f, 0f),
		new Vector3 (0f, 0f, 0.577f),
		new Vector3 (0.5f, 0f, 0.289f),
		new Vector3 (0.5f, 0f, -0.289f),
		new Vector3 (0f, 0f, -0.577f),
		new Vector3 (-0.5f, 0f, -0.289f),
		new Vector3 (-0.5f, 0f, 0.289f)};
	private int[] sideHeights;
	
	private int baseTex, tex;
	private float texWidth = 0.25f;
	private Vector2[] textures = new Vector2[] {
		new Vector2(0, 0), // Grass
		new Vector2(1, 0), // Dirt
		new Vector2(2, 0), // Water
		new Vector2(3, 0),
		new Vector2(0, 1),
		new Vector2(1, 1),
		new Vector2(2, 1),
		new Vector2(3, 1),
		new Vector2(0, 2),
		new Vector2(1, 2),
		new Vector2(2, 2),
		new Vector2(3, 2),
		new Vector2(0, 3),
		new Vector2(1, 3), // Move Target
		new Vector2(2, 3), // Attack Target
		new Vector2(3, 3)}; // Active

	void Awake() {
		overlayMats["base"] = Instantiate(Resources.Load ("Materials/TileMaterial")) as Material;
		overlayMats["base"].SetTexture ("_Overlaytex", null);
		overlayMats["moveTarget"] = Instantiate(Resources.Load ("Materials/TileMaterial")) as Material;
		overlayMats["moveTarget"].SetTexture("_OverlayTex", Resources.Load ("textures/tileOverlays/MoveTargetOverlayTex") as Texture);
		overlayMats["actionTarget"] = Instantiate(Resources.Load ("Materials/TileMaterial")) as Material;
		overlayMats["actionTarget"].SetTexture("_OverlayTex", Resources.Load ("textures/tileOverlays/ActionTargetOverlayTex") as Texture);
		overlayMats["active"] = Instantiate(Resources.Load ("Materials/TileMaterial")) as Material;
		overlayMats["active"].SetTexture("_OverlayTex", Resources.Load ("textures/tileOverlays/ActiveOverlayTex") as Texture);
		overlayMats["place"] = Instantiate(Resources.Load ("Materials/TileMaterial")) as Material;
		overlayMats["place"].SetTexture("_OverlayTex", Resources.Load ("textures/tileOverlays/PlaceOverlayTex") as Texture);
	}

	public static Tile Create(byte[] data) {
		return Tile.Create (new Point(data[0]-128, data[1]-128), data[2], data[3], data[4] == 1);
	}

	public static Tile Create(Point _pos, int _height, int _tex, bool _passable) {
		// Static generation method for tiles
		Tile tile = (Instantiate (prefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<Tile>();
		tile.pos = _pos;
		tile.height = _height;
		tile.baseTex = _tex;
		tile.tex = _tex;
		tile.passable = _passable;
		tile.transform.position = tile.GetCartesian ();
		tile.transform.name = "Tile - a:"+_pos.a+" b:"+_pos.b;
		return tile;
	}

	public Vector3 GetCartesian() {
		Vector3 newPos = pos.toPosition();
		return new Vector3(newPos.x, height * 0.25f, newPos.z);
	}

	#region Modifying tile
	public void SetOverlay() {
		transform.renderer.material = Tile.overlayMats["base"];
	}

	public void SetOverlay(string tex) {
		transform.renderer.material = Tile.overlayMats[tex];
	}

	public void SetTex () {
		tex = baseTex;
		mesh.uv = GetUVs ();
	}
	
	public void SetTex (int _tex) {
		tex = _tex;
		mesh.uv = GetUVs ();
	}
	#endregion
	#region Creating tile
	public void BuildTile () {
		// Creates the tile's mesh
		mesh = GetComponent<MeshFilter> ().mesh;
		meshCollider = GetComponent<MeshCollider> ();
		
		GetSideHeights ();
		
		mesh.Clear ();
		mesh.vertices = GetVertices ();
		mesh.uv = GetUVs ();
		mesh.triangles = GetTriangles ();
		mesh.Optimize ();
		mesh.RecalculateNormals ();
		meshCollider.sharedMesh = mesh;
	}

	private void GetSideHeights () {
		// Store how tall tile's sides are relative to the adjacent tiles
		sideHeights = new int[6];
		for (int side = 0; side < 6; side++) {
			Point otherTile = pos + new Point(dirs[side, 0], dirs[side, 1]);
			if (!(BattleController.tiles.ContainsKey(otherTile))) {
				sideHeights[side] = height;
			}
			else if (BattleController.tiles[otherTile].height != height)  {
				sideHeights[side] = height - BattleController.tiles[otherTile].height;
			}
			else {sideHeights[side] = 0;}
		}
	}

	private Vector3[] GetVertices () {
		// Return an array of the mesh's vertices
		List<Vector3> meshVertices = new List<Vector3> (); // Top vertices
		foreach (int vert in new int[] {0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 1}) { // Top vertices
			meshVertices.Add (hexagonPoints[vert]);
		}
		for (int i = 0; i < height; i++) { // Side vertices
			for (int side = 0; side < 6; side++) {
				if (i < sideHeights[side]) {
					meshVertices.Add(hexagonPoints[side+1] - new Vector3(0f, 0.25f*i, 0f));
					meshVertices.Add(hexagonPoints[side+1] - new Vector3(0f, 0.25f*(i+1), 0f));
					meshVertices.Add(hexagonPoints[(side+2) < 7 ? side+2 : 1] - new Vector3(0f, 0.25f*i, 0f));
					meshVertices.Add(hexagonPoints[(side+2) < 7 ? side+2 : 1] - new Vector3(0f, 0.25f*(i+1), 0f));
				}
			}
		}
		return meshVertices.ToArray();
	}
	
	private Vector2[] GetUVs () {
		// Return an array of the mesh's UVs
		List<Vector2> meshUV = new List<Vector2> (); // Top UVs
		meshUV.Add (new Vector2((0.5f + textures[tex].x) * texWidth, (0.5f + textures[tex].y) * texWidth)); // Top UVs
		for (int side = 0; side < 6; side++) {
			float noLineOffset = (sideHeights[side] != 0 || SaveState.settings.gridDisplay) ? 0.04f : 0.96f;
			meshUV.Add (new Vector2((0.211f + textures[tex].x) * texWidth, (textures[tex].y + noLineOffset) * texWidth));
			meshUV.Add (new Vector2((0.789f + textures[tex].x) * texWidth, (textures[tex].y + noLineOffset) * texWidth));
		}
		for (int i = 0; i < height; i++) { // Side UVs
			for (int side = 0; side < 6; side++) {
				if (i < sideHeights[side]) {
					int newTex = tex;
					if (i > 0) {newTex = 1;}
					meshUV.Add (new Vector2((0.98f + textures[newTex].x) * texWidth, (0.75f + textures[newTex].y) * texWidth));
					meshUV.Add (new Vector2((0.98f + textures[newTex].x) * texWidth, (0.25f + textures[newTex].y) * texWidth));
					meshUV.Add (new Vector2((0.02f + textures[newTex].x) * texWidth, (0.75f + textures[newTex].y) * texWidth));
					meshUV.Add (new Vector2((0.02f + textures[newTex].x) * texWidth, (0.25f + textures[newTex].y) * texWidth));
				}
			}
		}
		return meshUV.ToArray ();
	}
	
	private int[] GetTriangles () {
		// Return an array of the mesh's triangles
		List<int> meshTriangles = new List<int> (); // Top triangles
		foreach (int vert in new int[] {0, 1, 2, 0, 3, 4, 0, 5, 6, 0, 7, 8, 0, 9, 10, 0, 11, 12}) { // Top triangles
			meshTriangles.Add(vert);
		}
		int[] sideTriangles = new int[] {0, 1, 3, 0, 3, 2}; // Side triangles
		for (int i = 0; i < ((mesh.vertices.Length-13)/4)*6; i++) {
			meshTriangles.Add(13 + (i/6)*4 + sideTriangles[i%6]);
		}
		return meshTriangles.ToArray();
	}
	#endregion
}
