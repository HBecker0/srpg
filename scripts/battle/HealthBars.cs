﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HealthBars : MonoBehaviour {
	public static Dictionary<BatObj, HealthBar> healthBars = new Dictionary<BatObj, HealthBar>();

	public static void Init() {		
		GameObject healthBarPrefab = (GameObject)Resources.Load("Prefabs/HealthBar");
		foreach (BatObj b in BattleController.batObjs) {
			healthBars.Add (b, (HealthBar)((GameObject)Instantiate(Resources.Load("Prefabs/HealthBar"))).GetComponent<HealthBar>());
			healthBars[b].transform.parent = b.transform;
			healthBars[b].setReference(b);
			healthBars[b].Create();
		}
	}
}