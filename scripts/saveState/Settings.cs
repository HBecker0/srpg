﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Settings : MonoBehaviour {
	public bool gridDisplay = false;
	public float walkSpeed = 1.5f;
	public float turnSpeed = 5f;
	public float jumpSpeed = 2f;
	public Dictionary<string, KeyCode> keyBindings = new Dictionary<string, KeyCode> {
		{"cameraRotateUp", KeyCode.UpArrow},
		{"cameraRotateDown", KeyCode.DownArrow},
		{"cameraRotateLeft", KeyCode.LeftArrow},
		{"cameraRotateRight", KeyCode.RightArrow},
		{"cameraMoveUp", KeyCode.W},
		{"cameraMoveDown", KeyCode.S},
		{"cameraMoveLeft", KeyCode.A},
		{"cameraMoveRight", KeyCode.D},
		{"cameraZoomIn", KeyCode.Z},
		{"cameraZoomOut", KeyCode.X},
		{"back", KeyCode.Escape},
		{"confirm", KeyCode.Space}};
}
