﻿using UnityEngine;
using System.Collections;

public enum Directions {
	RightTop, // 1, 1
	Right, // 1,0
	RightBottom, // 0, -1
	LeftBottom, // -1, -1
	Left, // -1,0
	LeftTop // 0, 1
}

public static class DirectionsExtensions
{
	public static Directions GetDirection (this Point loc1, Point loc2) {
		Vector2 v1 = new Vector2(0,1);
		Vector2 v2 = loc2.toPosition().xz () - loc1.toPosition().xz();
		float angle = Vector2.Angle (v1, v2);
		if (Vector3.Cross (v1, v2).z > 0) {
			angle = 360 - angle;
		}
		int dir = Mathf.RoundToInt((angle - 30f)/60f);
		return (Directions)dir;
	}

	public static Vector3 GetVector(this Directions d) {
		return new Point(Tile.dirs[(int)d,0], Tile.dirs[(int)d, 1]).toPosition();
	}

	public static Vector3 ToEuler (this Directions d) {
		return new Vector3(0, (int)d * 60 + 30, 0);
	}

	public static Directions ToDirections (this Vector3 d) {
		return (Directions)(int)((d.y-30)/60);	
	}

	public static Directions ToDirections (this float y) {
		return (Directions)Mathf.RoundToInt((y-30f)/60f);	
	}
}