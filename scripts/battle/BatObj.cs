﻿	using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

public class BatObj : MonoBehaviour {
	private static GameObject prefab = Resources.Load ("prefabs/BatObj") as GameObject;
	private static Material material = Resources.Load ("materials/batObjMaterial") as Material;
	
	public string type;
	public Directions dir = Directions.Left;

	public Animator animator;
	public UnitData data;

	public Tile startPos;
	public Directions startDir;
	public bool moved;
	public bool acted;
	public Action currentAction = null;

	public static BatObj Create(string _type) {
		BatObj batObj = (Instantiate (prefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<BatObj>();
		batObj.type = _type;
		batObj.transform.name = _type;
		return batObj;
	}

	public static BatObj Create(string _type, byte[] _data) {
		BatObj batObj = Create (_type);
		batObj.data = new UnitData(_data);
		return batObj;
	}

	public static BatObj Create(string _type, string name) {
		BatObj batObj = Create (_type);
		batObj.data = SaveState.characters[name];
		return batObj;
	}

	public void BuildBatObj() {
		GameObject baseModelGO = Instantiate(Resources.Load<GameObject>("meshes/units/"+data.modelNames.Keys.ToArray()[0]), Vector3.zero, Quaternion.identity) as GameObject;
		GameObject baseMesh = baseModelGO.transform.GetChild (1).gameObject;
		baseMesh.layer = 9;
		baseMesh.transform.parent = transform;
		
		GameObject armature = baseModelGO.transform.GetChild (0).gameObject;
		armature.transform.parent = baseMesh.transform;
		Destroy (baseModelGO);
		
		(baseMesh.renderer as SkinnedMeshRenderer).material = material;
		baseMesh.renderer.material.mainTexture = Resources.Load<Texture>("textures/"+data.modelNames.Values.ToArray()[0]+"Tex");
		
		for (int i = 1; i < data.modelNames.Count; i++) {
			GameObject modelGO = Instantiate (Resources.Load<GameObject>("meshes/units/"+data.modelNames.Keys.ToArray()[i]), Vector3.zero, Quaternion.identity) as GameObject;
			GameObject mesh = modelGO.transform.GetChild (1).gameObject;
			mesh.layer = 9;
			mesh.transform.parent = transform;
			Destroy (modelGO);

			(mesh.renderer as SkinnedMeshRenderer).bones = (baseMesh.renderer as SkinnedMeshRenderer).bones;
			(mesh.renderer as SkinnedMeshRenderer).rootBone = (baseMesh.renderer as SkinnedMeshRenderer).rootBone;//armature.transform;
			(mesh.renderer as SkinnedMeshRenderer).material = material;
			(mesh.renderer as SkinnedMeshRenderer).localBounds = (baseMesh.renderer as SkinnedMeshRenderer).localBounds;
			mesh.renderer.material.mainTexture = Resources.Load<Texture>("textures/"+data.modelNames.Values.ToArray()[i]+"Tex");
		}
		
		animator = this.GetComponent<Animator>();
		if (animator != null) {
			animator.avatar = (baseModelGO.GetComponent<Animator>() as Animator).avatar;
			animator.runtimeAnimatorController = Resources.Load ("animators/"+data.modelNames.Keys.ToArray()[0]) as RuntimeAnimatorController;
		}

		UpdateRotation();
	}

	public void UpdateRotation () {
		transform.localRotation = Quaternion.Euler(dir.ToEuler());
	}

	public Point GetLocation() {
		return transform.parent.GetComponent<Tile>().pos;
	}

	public Tile GetTile() {
		return transform.parent.GetComponent<Tile>();
	}

	public void ChangeHP(int amount) {
		data.HP += amount;
		if (data.HP > data.maxHP) {
			data.HP = data.maxHP;
		}
		if (data.HP <= 0) {
			data.HP = 0;
			BattleController.KillUnit (this);
			StartCoroutine(Die ());
		}
	}

	IEnumerator Die() {
		animator.SetBool("dying", true);
		while (!GetComponent<BatObj>().animator.GetNextAnimatorStateInfo(0).IsName ("dead")) {
			yield return null;
		}
		HealthBars.healthBars.Remove ((BatObj)this);
		Destroy(this.gameObject);
	}
}
