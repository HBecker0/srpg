﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class UnitData {
	public string name;
	public Dictionary<string, string> modelNames = new Dictionary<string, string>();
	public int maxHP;
	public int HP;
	public int speed;
	public int moveDist;
	public int jumpHeight;

	public UnitData(byte[] data) {
		name = "Unit";
		for (int i = 0; i < data[0]; i++) {
			modelNames.Add(Encoding.UTF8.GetString(data.Segment(1+i*4, 4)), Encoding.UTF8.GetString(data.Segment(1+data[0]*4+i*4, 4)));
		}
		maxHP = data[1 + 8 * data[0]];
		HP = maxHP;
		speed = 8;
		moveDist = 3;
		jumpHeight = 3;
	}

	public UnitData() {
		speed = 8;
		moveDist = 3;
		jumpHeight = 3;
	}
}
