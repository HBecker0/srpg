﻿using UnityEngine;
using System.Collections;

public abstract class BattleState : State {
	protected BattleController owner;
	public CameraController cameraController { get { return owner.cameraController; }}
	public Level level { get { return owner.level; }}
	public GameObject hover { get { return owner.hover; }}
	public Tile hoveredTile { get { return owner.hoveredTile; } set {owner.hoveredTile = value;}}
	public BatObj activeUnit { get { return owner.activeUnit; } set {owner.activeUnit = value;}}

	public LayerMask layerMask = (1<<8); // Tile layer, for mouseover raycasting

	protected void Awake () {
		owner = GetComponent<BattleController>();
	}
	
	protected override void AddListeners () {
		InputController.keyEvent += OnKeyEvent;
		InputController.keyDownEvent += OnKeyDownEvent;
		InputController.mouseButtonUpEvent += OnMouseButtonUpEvent;
		InputController.mouseButtonDownEvent += OnMouseButtonDownEvent;
		InputController.mouseAxisEvent += OnMouseAxisEvent;
		InputController.mouseScrollEvent += OnMouseScrollEvent;
	}
	
	protected override void RemoveListeners () {
		InputController.keyEvent -= OnKeyEvent;
		InputController.keyDownEvent -= OnKeyDownEvent;
		InputController.mouseButtonUpEvent -= OnMouseButtonUpEvent;
		InputController.mouseButtonDownEvent -= OnMouseButtonDownEvent;
		InputController.mouseAxisEvent -= OnMouseAxisEvent;
		InputController.mouseScrollEvent -= OnMouseScrollEvent;
	}
	
	protected virtual void OnKeyEvent (object sender, InfoEventArgs<string> e) {
		cameraController.KeyEvent(e.info);
	}

	protected virtual void OnKeyDownEvent (object sender, InfoEventArgs<string> e) {

	}

	protected virtual void OnMouseButtonUpEvent (object sender, InfoEventArgs<int> e) {
		cameraController.MouseButtonUpEvent(e.info);
	}

	protected virtual void OnMouseButtonDownEvent (object sender, InfoEventArgs<int> e) {
		cameraController.MouseButtonDownEvent(e.info);
	}

	protected virtual void OnMouseAxisEvent (object sender, InfoEventArgs<Vector2> e) {
		cameraController.MouseAxisEvent(e.info);
	}

	protected virtual void OnMouseScrollEvent (object sender, InfoEventArgs<float> e) {
		cameraController.MouseScrollEvent(e.info);
	}

	void Update() {
		HandleMouseOver();
	}
	
	private void HandleMouseOver() {
		// Find if the cursor is over a tile and set hoveredTile to it if so
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Point newTile = new Point();
		
		if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask)) {
			Vector3 hitpoint = hit.point;
			if (((hit.point.y * 4f) != (float)(int)(hit.point.y * 4f))) { // If touching a tile's side
				hitpoint = hit.point + (ray.direction * 0.02f); // Move the hit point forward slightly
			}
			if (BattleController.tiles.ContainsKey(hitpoint.toLocation()) && !((((float)BattleController.tiles[hitpoint.toLocation()].height)/4f - hit.point.y) > 0.25f)) { // Not touching a BaseTile
				newTile = hitpoint.toLocation();
			}
		}
		if (((hoveredTile != null) && (newTile != hoveredTile.pos) || hoveredTile == null)) {
			hoveredTile = BattleController.tiles[newTile];
			moveHover();
		}
	}
	
	private void moveHover() {
		if (hoveredTile == null) {
			hover.transform.position = new Vector3(0, -100, 0);
		} else {
			hover.transform.position = hoveredTile.GetCartesian() + new Vector3(0, 0.01f, 0);
		}
	}
}