﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SelectActionBattleState : BattleState {
	RectTransform rectTransform;
	List<Text> textItems;
	List<MyMenuItem> actions;
	public static int x;

	public override void Enter () {
		base.Enter ();
		rectTransform = transform.GetChild (0).FindChild ("ActionMenu").transform as RectTransform;
		rectTransform.gameObject.SetActive (true);
		textItems = new List<Text>();
		for (int i = 0; i < 5; i++) {
			textItems.Add (rectTransform.GetComponentsInChildren<Text>()[i]);
		}
		for (int i = 0; i < 5; i++) {
			textItems[i].color = Color.black;
		}
		GenActions();
		for (int i = 0; i < 5; i++) {
			textItems[i].text = actions[i].text;
		}
	}

	public override void Exit() {
		base.Exit ();
		rectTransform.gameObject.SetActive (false);
	}

	private void GenActions() {
		actions = new List<MyMenuItem>();
		actions.Add(new MyMenuItem((activeUnit.moved ? "Undo " : "") + "Move", () => owner.ChangeState<MoveBattleState>()));
		actions.Add(new MyMenuItem("Attack", () => SelectAttack()));
		actions.Add(new MyMenuItem("Grimoires", () => owner.ChangeState<SelectGrimoireBattleState>()));
		actions.Add(new MyMenuItem("Command", () => owner.ChangeState<CommandBattleState>()));
		actions.Add(new MyMenuItem("End Turn", () => owner.ChangeState<TurnBattleState>()));
		if (activeUnit.acted) {
			for (int i = 1; i < 4; i++) {
				textItems[i].color = Color.grey;
				actions[i].active = false;
			}
			if (activeUnit.moved) {
				textItems[0].color = Color.grey;
				actions[0].active = false;
			}
		}
	}

	public void SelectAttack() {
		activeUnit.currentAction = new Action();
		owner.ChangeState<TargetBattleState>();
	}

	public void SelectItem(int item) {
		actions[item].Click();
	}
}

public class MyMenuItem {
	public bool active = true;
	public string text;
	public ClickItem<Type> click;
	public MyMenuItem(string _text, ClickItem<Type> _click) {
		text = _text;
		click = _click;
	}
	public void Click() {
		if (active) {
			click();
		}
	}
}

public delegate void ClickItem<T>() where T : State;
