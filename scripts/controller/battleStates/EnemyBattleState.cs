﻿using UnityEngine;
using System.Collections;

public class EnemyBattleState : BattleState {
	
	public override void Enter () {
		base.Enter();
		Debug.Log ("Enemy");
		StartCoroutine(Back());
	}
	
	IEnumerator Back () {
		yield return null;
		owner.ChangeState<TurnBattleState>();
	}
}
