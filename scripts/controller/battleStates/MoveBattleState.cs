﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MoveBattleState : BattleState {
	public Dictionary<Point, List<Point>> routes;
	Movement mover;
	bool returnFlag = false;

	public override void Enter () {
		base.Enter ();
		if (activeUnit.moved) {
			activeUnit.GetTile ().SetOverlay ("base");
			activeUnit.transform.parent = activeUnit.startPos.transform;
			activeUnit.transform.localPosition = Vector3.zero;
			activeUnit.dir = activeUnit.startDir;
			activeUnit.UpdateRotation();
			activeUnit.GetTile ().SetOverlay ("active");
			activeUnit.moved = false;
			returnFlag = true;
		} else {
			routes = genRoutes ();
			mover = activeUnit.GetComponent<Movement>() as Movement;
			foreach (Point point in routes.Keys.ToArray()) {
				BattleController.tiles[point].SetOverlay ("moveTarget");
			}
			activeUnit.startPos = activeUnit.GetTile ();
			activeUnit.startDir = activeUnit.dir;
		}
	}

	public override void Exit() {
		base.Exit ();
		if (routes != null && !activeUnit.moved) {
			foreach (Point point in routes.Keys.ToArray()) {
				BattleController.tiles[point].SetOverlay ("base");
			}
		}
	}

	protected override void OnMouseButtonDownEvent (object sender, InfoEventArgs<int> e) {
		base.OnMouseButtonDownEvent(sender, e);
		if (e.info == 0 && !mover.active && !activeUnit.moved) {
			if (hoveredTile != null) {
				if (routes.ContainsKey (hoveredTile.pos)) {
					List<Tile> route = new List<Tile>() {activeUnit.GetTile()};
					for (int i = 0; i < routes[hoveredTile.pos].Count; i++) {
						route.Add (BattleController.tiles[routes[hoveredTile.pos][i]]);
					}
					foreach (Point point in routes.Keys.ToArray()) {
						BattleController.tiles[point].SetOverlay ("base");
					}
					activeUnit.GetTile ().SetOverlay ("base");
					StartCoroutine(mover.Move (route));
					activeUnit.moved = true;
					returnFlag = true;
				}
			}
		}
	}

	protected override void OnKeyDownEvent (object sender, InfoEventArgs<string> e) {
		base.OnKeyDownEvent(sender, e);
		if (e.info == "back") {
			if (mover.active) {
				mover.StopAllCoroutines();
				mover.active = false;
			}
			if (activeUnit.moved) {
				activeUnit.GetTile ().SetOverlay ("base");
				activeUnit.transform.parent = activeUnit.startPos.transform;
				activeUnit.dir = activeUnit.startDir;
				activeUnit.UpdateRotation ();
				activeUnit.transform.localPosition = Vector3.zero;
				activeUnit.GetTile ().SetOverlay ("active");
				activeUnit.moved = false;
			}
			owner.ChangeState<SelectActionBattleState>();
		}
	}

	void Update() {
		if (returnFlag && !mover.active) {
			returnFlag = false;
			owner.ChangeState<SelectActionBattleState>();
		}
	}

	public Dictionary<Point, List<Point>> genRoutes() {
		Dictionary<Point, List<Point>> routes = RouteSearch.BFS (activeUnit.GetLocation(), activeUnit.data.moveDist, (Point t) => CheckCanMove(t));
		foreach (BatObj obj in  BattleController.batObjs) {
			if (obj.type == "PU" || obj.type == "EU") {
				if (routes.ContainsKey (obj.GetLocation())) {
					routes.Remove (obj.GetLocation());
				}
			}
		}
		return routes;
	}
	
	public bool CheckCanMove(Point tile) {
		bool moveable = false;
		if (BattleController.tiles[tile].passable) { //Tile passable
			if (BattleController.tiles[tile].height <= (BattleController.tiles[tile].height + activeUnit.data.jumpHeight)) { //Tile not too high
				if (BattleController.tiles[tile].transform.childCount == 0) {
					moveable = true;
				}
				for (int i = 0; i < BattleController.tiles[tile].transform.childCount; i++) {
					if (BattleController.tiles[tile].transform.GetChild (i).GetComponent<BatObj>().type == activeUnit.type) {
						moveable = true;
					}
				}
			}
		}
		return moveable;
	}
}