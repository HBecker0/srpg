﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour {
	GameObject border;
	GameObject bar;
	public BatObj owner;
	int value;

	public void Create() {
		renderer.material = Instantiate(Resources.Load ("materials/flat")) as Material;
		renderer.material.color = new Color(1f, 1f, 1f, 0.5f);
		renderer.sortingOrder = 1;
		renderer.sortingLayerID = 1;
		renderer.sortingLayerName = "MainSortingLayer";
		border = transform.GetChild (0).gameObject;
		border.renderer.material = Instantiate(Resources.Load ("materials/flat")) as Material;
		border.renderer.material.color = new Color(0f, 0f, 0f, 0.5f);
		border.renderer.sortingOrder = 3;
		border.renderer.sortingLayerID = 1;
		border.renderer.sortingLayerName = "MainSortingLayer";
		bar = border.transform.GetChild (0).gameObject;
		bar.renderer.material = Instantiate(Resources.Load ("materials/flat")) as Material;
		bar.renderer.material.color = new Color(0f, 0.8f, 0f, 0.5f);
		bar.renderer.sortingOrder = 2;
		bar.renderer.sortingLayerID = 1;
		bar.renderer.sortingLayerName = "MainSortingLayer";

		if (owner.type == "EU") {
			bar.renderer.material.color = new Color(0.8f, 0.2f, 0.2f, 0.5f);
		} else if (owner.type == "PU") {
			bar.renderer.material.color = new Color(0.2f, 0.2f, 0.8f, 0.5f);
		}

		value = owner.data.HP;
	}

	public void setReference(BatObj _owner) {
		owner = _owner;
		transform.position = owner.transform.position;
	}

	void Update() {
		transform.rotation = Quaternion.LookRotation (-Camera.main.transform.forward);
		if (owner.data.HP != value) {
			bar.transform.localScale = new Vector3(((float)owner.data.HP)/owner.data.maxHP, 1, 1);
			bar.transform.transform.localPosition = new Vector3((1f - ((float)owner.data.HP)/owner.data.maxHP)/2f, 0, 0);
		}
	}
}
