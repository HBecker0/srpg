﻿using UnityEngine;
using System.Collections;

public static class byteArrayExtensions {
	public static byte[] Segment(this byte[] array, int start, int length) {
		byte[] bb =  new byte[length];
		for (int i = 0; i < length; i++) {
			bb[i] = array[start+i];
		}
		return bb;
	}

	public static byte[] Segment(this byte[] array, int start) {
		byte[] bb =  new byte[array.Length-start];
		for (int i = 0; i < array.Length-start; i++) {
			bb[i] = array[start+i];
		}
		return bb;
	}
}