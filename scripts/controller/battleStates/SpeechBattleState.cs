﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SpeechBattleState : BattleState {
	Queue<SpeechData> data;
	RectTransform rectTransform;
	Image face;
	Text text;
	RectTransform indicator;
	bool onScreen = false;

	public override void Enter () {
		base.Enter ();
		data = level.speechData["intro"];
		rectTransform = transform.GetChild (0).FindChild ("Speech").transform as RectTransform;
		rectTransform.gameObject.SetActive (true);
		face = GetComponentsInChildren<Image>()[1];
		text = GetComponentInChildren<Text>();
		indicator = GetComponentsInChildren<Image>()[2].rectTransform;
		rectTransform.anchoredPosition = new Vector2(0, -200);
		StartCoroutine (MoveOnScreen());

		SpeechData item = data.Dequeue();
		if (data.Count == 0) {
			indicator.gameObject.SetActive (false);
		}
		face.sprite = item.face;
		text.text = item.text;
		StartCoroutine("MoveIndicator");
	}

	public override void Exit() {
		base.Exit();
		StopCoroutine("MoveIndicator");
	}
	protected override void OnKeyDownEvent (object sender, InfoEventArgs<string> e) {
		base.OnKeyDownEvent(sender, e);
		if (e.info == "confirm") {
			if (onScreen) {
				if (data.Count > 0) {
					SpeechData item = data.Dequeue();
					face.sprite = item.face;
					text.text = item.text;
					if (data.Count == 0) {
						indicator.gameObject.SetActive (false);
					}
				} else {
					StartCoroutine (MoveOffScreen());
				}
			}
		}
	}

	IEnumerator MoveOnScreen() {
		while (rectTransform.anchoredPosition.y < 150) {
			rectTransform.anchoredPosition = new Vector2(0, rectTransform.anchoredPosition.y + 350f * Time.deltaTime * 3f);
			yield return null;
		}
		rectTransform.anchoredPosition = new Vector2(0, 150);
		onScreen = true;
	}

	IEnumerator MoveOffScreen() {
		while (rectTransform.anchoredPosition.y > -200) {
			rectTransform.anchoredPosition = new Vector2(0, rectTransform.anchoredPosition.y - 350f * Time.deltaTime * 3f);
			yield return null;
		}
		rectTransform.anchoredPosition = new Vector2(0, 200);
		rectTransform.gameObject.SetActive (false);
		onScreen = false;

		owner.ChangeState<PlaceBattleState>();
	}

	IEnumerator MoveIndicator() {
		int offset = 0;
		int direction = 1;
		while (true) {
			indicator.anchoredPosition = new Vector2(0, indicator.anchoredPosition.y + direction);
			offset++;
			if (offset > 20) {
				direction *= -1;
				offset = 0;
			}
			yield return null;
		}

	}
}

public struct SpeechData {
	public Sprite face;
	public string text;
	public SpeechData (Sprite _face, string _text) {
		face = _face;
		text = _text;
	}
}