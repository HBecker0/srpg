﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Movement : MonoBehaviour {
	private Tile current;
	private Tile target;
	public bool active = false;

	public IEnumerator Move (List<Tile> route) {
		active = true;
		GetComponent<BatObj>().animator.SetBool ("walking", true);
		for (int i = 1; i < route.Count; i++) {
			current = route[i-1];
			target = route[i];
			Directions dir = current.pos.GetDirection(target.pos);
			if (GetComponent<BatObj>().dir != dir) {
				yield return StartCoroutine(Turn(dir));
				GetComponent<BatObj>().dir = dir;
				GetComponent<BatObj>().UpdateRotation();
			}
			if (current.height == target.height) {
				yield return StartCoroutine(Walk(dir));
				transform.parent = target.transform;
				transform.localPosition = Vector3.zero;
			} else {
				yield return StartCoroutine(Jump(dir, target.height - current.height));
				transform.parent = target.transform;
				transform.localPosition = Vector3.zero;
			}
		}
		
		yield return null;
		active = false;
		GetComponent<BatObj>().animator.SetBool ("walking", false);
		GetComponent<BatObj>().GetTile ().SetOverlay ("active");
	}

	IEnumerator Turn (Directions targetD) {
		float current = GetComponent<BatObj>().dir.ToEuler().y;
		float target = targetD.ToEuler().y;
		float right = target - current;
		float left = current - target;
		if (target < current) {
			right += 360f;
		} else {
			left += 360f;
		}
		int rotateDirection = (int)Mathf.Sign (left - right);
		int side = 0;
		while (true) {
			if (side == 0 && Mathf.Abs (target-transform.eulerAngles.y) < 30f) {
				side = (int)Mathf.Sign (transform.eulerAngles.y - target);
			}
			transform.Rotate (Vector3.up, rotateDirection * 60f * Time.deltaTime * SaveState.settings.turnSpeed);
			HealthBar healthBar = GetComponentInChildren<HealthBar>() as HealthBar;
			healthBar.transform.rotation = Quaternion.LookRotation (-Camera.main.transform.forward);
			yield return null;
			if (side != 0 && side != (int)Mathf.Sign (transform.eulerAngles.y - target)) {
				break; 
			}
		}
	}

	IEnumerator Walk (Directions dir) {
		Vector3 targetPos = dir.GetVector();
		while (transform.localPosition != targetPos) {
			transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPos, Time.deltaTime * SaveState.settings.walkSpeed);
			yield return null;
		}
	}
	
	IEnumerator Jump (Directions dir, float height) {
		GetComponent<BatObj>().animator.SetBool ("jumping", true);
		Vector3 target = dir.GetVector();
		target.y = height*0.25f;
		Vector3 middle = new Vector3(target.x/2, 0.25f, target.z/2);
		if (height > 0) {
			middle.y += height*0.25f;
		}
		Vector3 coefficients = CalcParabolaCoefficients(middle.x, middle.y, target.x, target.y);
		float distMoved = 0;
		while (distMoved < 1) {
			float dist = Time.deltaTime * SaveState.settings.jumpSpeed;
			distMoved += Mathf.Sqrt (Mathf.Pow (target.x * dist, 2) + Mathf.Pow (target.z * dist, 2));
			transform.localPosition = new Vector3(target.x*distMoved, coefficients.x*Mathf.Pow(transform.localPosition.x, 2) + coefficients.y*transform.localPosition.x, target.z*distMoved);
			yield return null;
		}
		GetComponent<BatObj>().animator.SetBool ("jumping", false);
	}

	public Vector2 CalcParabolaCoefficients(float x1, float y1, float x2, float y2) {
		return new Vector2((x2*y1 - x1*y2)/(x1*x1*x2 - x1*x2*x2), (x1*x1*y2 - x2*x2*y1)/(x1*x2*(x1-x2)));
	}
}
