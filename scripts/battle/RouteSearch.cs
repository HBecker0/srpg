﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RouteSearch {
	public static Dictionary<Point, List<Point>> BFS(Point start, int distance, Condition addCondition) {
		Dictionary<Point, List<Point>> tempRoutes = new Dictionary<Point, List<Point>>() {{start, new List<Point>()}};
		List<Point> current = new List<Point>() {start};
		List<Point> nextItr = new List<Point>();

		for (int i = 1; i <= distance; i++) {
			foreach (Point tile in current) {
				for (int j = (i%2 == 1 ? 5 : 0); j != (i%2 == 1 ? -1 : 6); j += (i%2 == 1 ? -1 : 1)) {
					Point adjTile = tile + new Point(Tile.dirs[j, 0], Tile.dirs[j, 1]);
					if (BattleController.tiles.ContainsKey(adjTile)) { //Tile present
						if (!(tempRoutes.ContainsKey(adjTile))) { //Tile not checked
							if (addCondition(adjTile)) {
								nextItr.Add(adjTile);
								if (tempRoutes.ContainsKey (tile)) {
									tempRoutes.Add (adjTile, new List<Point>(tempRoutes[tile]));
								} else {
									tempRoutes.Add (adjTile, new List<Point>());
								}
								tempRoutes[adjTile].Add(adjTile);
							}
						}
					}
				}
			}
			current = new List<Point>(nextItr);
			nextItr = new List<Point>();
		}
		return tempRoutes;
	}
}

public delegate bool Condition(Point tile);