﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlaceBattleState : BattleState {
	private int placeSelection = 0;
	private List<string> placedUnits = new List<string>();

	RectTransform rectTransform;
	RectTransform windowBottom;
	RectTransform placingIndicator;
	GameObject placeBlock;
	bool onScreen = false;

	public override void Enter () {
		base.Enter ();
		rectTransform = transform.GetChild (0).FindChild ("Place").transform as RectTransform;
		windowBottom = rectTransform.FindChild ("BackgroundBottom") as RectTransform;
		placingIndicator = rectTransform.FindChild ("PlacingIndicator") as RectTransform;
		placeBlock = (Resources.Load("prefabs/PlaceBlock") as GameObject);

		rectTransform.gameObject.SetActive (true);

		for (int i = 0; i < SaveState.characters.Count; i++) {
			RectTransform block = (Instantiate (placeBlock) as GameObject).transform as RectTransform;
			block.transform.parent = rectTransform.FindChild ("Blocks").transform;
			block.transform.name = SaveState.characters.Keys.ToArray()[i];
			EventTrigger trigger = block.GetComponent<EventTrigger>();
			EventTrigger.Entry entry = new EventTrigger.Entry();
			entry.eventID = EventTriggerType.PointerClick;
			entry.callback = new EventTrigger.TriggerEvent();
			int q = i;
			UnityEngine.Events.UnityAction<BaseEventData> callback = new UnityEngine.Events.UnityAction<BaseEventData>((f) => SelectItem (f, q));
			entry.callback.AddListener (callback);
			trigger.delegates.Add (entry);

			block.anchoredPosition = new Vector2(120, -(100 * (i+1)));
			windowBottom.anchoredPosition = new Vector2(windowBottom.anchoredPosition.x, windowBottom.anchoredPosition.y - 100);
		}

		for (int i = 0; i < level.PUStartingTiles.Count; i++) {
			level.PUStartingTiles[i].SetOverlay("place");
		}
	}

	public override void Exit () {
		base.Exit ();
		rectTransform.gameObject.SetActive (false);
	}

	protected override void OnMouseButtonDownEvent (object sender, InfoEventArgs<int> e) {
		base.OnMouseButtonDownEvent(sender, e);
		if (level.PUStartingTiles.Contains (hoveredTile)) {
			if (placeSelection != -1) {
				// If the unit is already placed elsewhere, destroy that instance of it
				if (placedUnits.Contains (SaveState.characters.Keys.ToArray ()[placeSelection])) {
					foreach (BatObj u in BattleController.batObjs) {
						if (u.data.name == (SaveState.characters.Keys.ToArray ()[placeSelection])) {
							RemoveUnit (u.GetTile ());
							placedUnits.Remove (SaveState.characters.Keys.ToArray ()[placeSelection]);
							break;
						}
					}
				}
				// Destroy the unit on the selected tile if there is one
				string removed = RemoveUnit (hoveredTile);
				if (removed != "") {
					placedUnits.Remove (removed);
					rectTransform.FindChild ("Blocks").FindChild (removed).GetChild (1).gameObject.SetActive(false);
				}
				// Create the new unit on the select tile
				BatObj batObj = BatObj.Create("PU", SaveState.characters.Keys.ToList ()[placeSelection]);
				batObj.transform.parent = hoveredTile.transform;
				BattleController.batObjs.Add (batObj);
				batObj.BuildBatObj();
				batObj.transform.localPosition = Vector3.zero;
				placedUnits.Add (SaveState.characters.Keys.ToArray ()[placeSelection]);
				rectTransform.FindChild ("Blocks").GetChild (placeSelection).GetChild (1).gameObject.SetActive(true);
			}
		}
	}

	public string RemoveUnit(Tile tile) {
		BatObj obj = tile.GetComponentInChildren<BatObj>();
		if (obj != null) {
			BattleController.batObjs.Remove(obj);
			Destroy (obj.gameObject);
			return obj.data.name;
		}
		return "";
	}

	public void ConfirmPlacement() {
		if (placedUnits.Count > 0) {
			if (placedUnits.Count < SaveState.characters.Count) {
				// Ask player to confirm they don't want to use all units
			} else {
				// Ask player to confirm placement
			}
			CompletePlacement(); // "Yes" answer to confirmations should call this function, not here
		}
	}

	public void CompletePlacement() {
		foreach (Tile tile in level.PUStartingTiles) {
			tile.SetOverlay("base");
		}
		HealthBars.Init();
		owner.ChangeState<TurnBattleState>();
	}

	public void SelectItem(UnityEngine.EventSystems.BaseEventData data, int i) {
		placeSelection = i;
		placingIndicator.anchoredPosition = new Vector2(120, -(100 * (i+1)));

	}
}